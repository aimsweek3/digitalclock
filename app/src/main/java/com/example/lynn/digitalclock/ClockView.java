package com.example.lynn.digitalclock;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

import static com.example.lynn.digitalclock.MainActivity.*;

/**
 * Created by lynn on 6/21/2016.
 */
public class ClockView extends LinearLayout {

    public ClockView(Context context) {
        super(context);

        setBackground(getResources().getDrawable(R.drawable.background));

        drawables = new Drawable[10];

        drawables[0] = getResources().getDrawable(R.drawable.zero);
        drawables[1] = getResources().getDrawable(R.drawable.one);
        drawables[2] = getResources().getDrawable(R.drawable.two);
        drawables[3] = getResources().getDrawable(R.drawable.three);
        drawables[4] = getResources().getDrawable(R.drawable.four);
        drawables[5] = getResources().getDrawable(R.drawable.five);
        drawables[6] = getResources().getDrawable(R.drawable.six);
        drawables[7] = getResources().getDrawable(R.drawable.seven);
        drawables[8] = getResources().getDrawable(R.drawable.eight);
        drawables[9] = getResources().getDrawable(R.drawable.nine);

        colon = getResources().getDrawable(R.drawable.colon);

        am = getResources().getDrawable(R.drawable.am);
        pm = getResources().getDrawable(R.drawable.pm);

        views = new ImageView[9];

        for (int counter=0;counter<views.length;counter++) {
            views[counter] = new ImageView(context);

            views[counter].setBackgroundColor(0xFF000000);

            views[counter].setImageDrawable(drawables[0]);

            addView(views[counter]);
        }

        views[2].setImageDrawable(colon);
        views[5].setImageDrawable(colon);
        views[8].setImageDrawable(pm);

        TextView timeZone = new TextView(context);

        timeZone.setText("Time Zone: ");

        timeZone.setTextSize(40);

        addView(timeZone);

        List<String> list = new ArrayList<>();

        list.addAll(Arrays.asList(TimeZone.getAvailableIDs()));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item,list) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view =super.getView(position, convertView, parent);
                TextView textView=(TextView) view.findViewById(android.R.id.text1);
                // do whatever you want with this text view
                textView.setTextSize(20);
                return view;
            }


        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        timeZones = new Spinner(context);

        timeZones.setAdapter(adapter);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(400,200);

        timeZones.setLayoutParams(layoutParams);

        addView(timeZones);

        new MyThread();
    }

}
