package com.example.lynn.digitalclock;

import java.util.Calendar;
import java.util.TimeZone;

import static com.example.lynn.digitalclock.MainActivity.*;

/**
 * Created by lynn on 6/21/2016.
 */
public class MyThread implements Runnable {
    private Thread thread;
    private boolean keepGoing;

    public MyThread() {
        thread = new Thread(this);

        keepGoing = true;

        thread.start();
    }


    public void getTime() {
        Calendar c = Calendar.getInstance();

        TimeZone timeZone = TimeZone.getTimeZone(timeZones.getSelectedItem().toString());

        c.setTimeZone(timeZone);

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);

        final boolean theAm = hour < 12;

        hour -= (hour > 12) ? 12 : 0;

        final int hourFirstDigit = hour/10;
        final int hourSecondDigit = hour%10;

        final int minuteFirstDigit = minute/10;
        final int minuteSecondDigit = minute%10;

        final int secondFirstDigit = second/10;
        final int secondSecondDigit = second%10;

        views[0].post(new Runnable() {

            @Override
            public void run() {
               views[0].setImageDrawable(drawables[hourFirstDigit]);
                views[1].setImageDrawable(drawables[hourSecondDigit]);
                views[3].setImageDrawable(drawables[minuteFirstDigit]);
                views[4].setImageDrawable(drawables[minuteSecondDigit]);
                views[6].setImageDrawable(drawables[secondFirstDigit]);
                views[7].setImageDrawable(drawables[secondSecondDigit]);
                views[8].setImageDrawable((theAm) ? am : pm);
            }
        });
    }

    public void pause(double seconds) {
        try {
            Thread.sleep((int)(seconds*1000));
        } catch (InterruptedException ie) {
            System.out.println(ie);
        }
    }

    @Override
    public void run() {
        while (keepGoing) {
            getTime();

            pause(0.5);
        }
    }

}
